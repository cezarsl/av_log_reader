from flask import Flask, render_template
import os
from app.parse_logs import log_parser, merge_logs

app = Flask(__name__)


def get_log_info():
    parsed_logs = dict()
    for ver in os.listdir(app.static_folder):
        parsed_logs[ver] = []
        for log in os.listdir(os.path.join(app.static_folder, ver)):
            parsed_logs[ver].append(log_parser(
                os.path.join(app.static_folder, ver, log)))
        parsed_logs[ver] = merge_logs(parsed_logs[ver])

    return parsed_logs


def get_platforms(v):
    return sorted(list(map(lambda x: int(os.path.splitext(
        x)[0]), os.listdir(os.path.join(app.static_folder, v)))))


@app.route('/')
def _home():
    versions = os.listdir(app.static_folder)
    log_info = get_log_info()
    data = dict()
    for v in versions:
        platforms = get_platforms(v)
        data[v] = {
            "platforms": platforms,
            "total": sum([log_info[v][t][p]["sr_test_cases"] for t in log_info[v] for p in platforms]),
            "failed": sum([log_info[v][t][p]["sr_tests_failed"] for t in log_info[v] for p in platforms])
        }
    return render_template("home.j2", data=data)


@app.route('/<ver>')
def _version(ver):
    platforms = get_platforms(ver)
    return render_template("version.j2", data=get_log_info()[ver], ver=ver, platforms=platforms)


@app.route('/<ver>/<test>/<platform>')
def _test_per_platform(ver, test, platform):
    return render_template("platform.j2",
                           test=get_log_info()[ver][test]["sr_test_name"],
                           platform=platform,
                           data=get_log_info()[ver][test][int(platform)])
