import json
from collections import OrderedDict
# from urllib.parse import quote
import os


def log_parser(log_path):
    with open(log_path) as log_file:
        data = json.load(log_file)["data"]

    return data


def color(line):
    if line.startswith("#"):
        return

    if line.startswith("not ok") or "error" in line:
        return "red"

    if "skip" in line:
        return "blue"

    if line.startswith("ok"):
        return "green"


def parse_tap(tap_data):
    return [
        [
            color(line.lower()),
            line
        ]
        for line in tap_data
    ]


def merge_logs(logs):
    combined = OrderedDict()
    for log in logs:
        for data in log:
            name = data["sr_test_name"].replace(os.path.sep, "_")
            combined[name] = combined.get(name, dict())

            combined[name][data["sr_ts_id"]] = {
                "sr_tests_failed": data["sr_tests_failed"],
                "sr_tap": parse_tap(data["sr_tap"].split("\n")),
                "sr_tap_raw": data["sr_tap"],
                "sr_test_cases": data["sr_test_cases"],
            }

            combined[name]["sr_test_name"] = data["sr_test_name"]

    def _total_failed(test_dict):
        return sum([v["sr_tests_failed"] for v in test_dict.values() if isinstance(v, dict)])

    sorted_combined = OrderedDict(
        sorted(combined.items(), key=lambda x: _total_failed(x[1]), reverse=True))
    return sorted_combined
