To Run

1. Create venv
$ python3 -m venv venv
2. Activate venv
$ source venv/bin/activate
3. Install Requirements
$ python -m pip install -r requirements.txt
4. Use the run script
$ ./run.sh
5. Open localhost:5000 in Browser